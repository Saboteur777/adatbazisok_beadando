<?php include('../config/functions.php');?>
<input type="hidden" name="addType" id="addType" value="Patches">
<div class="form-group col-xs-12">
  <input type="name" class="form-control input-lg" id="name" name="name" placeholder="Patch name">
</div>
<div class="form-group col-xs-12">
  <textarea rows="8" class="form-control" name="description" id="description" placeholder="Please explain how you solved the issue with this new Patch!"></textarea>
</div>
<div class="form-group col-xs-12">
  <textarea rows="8" class="form-control" name="code" id="code" placeholder="Paste your code here!"></textarea>
</div>
<div class="form-group col-xs-12">
  <?php $query = table_content('Tickets', $conn); ?>
  <?php if ($query) { ?>
    <label for="ticket_id">Please select an open Ticket!</label>
    <select id="ticket_id" name="ticket_id" class="form-control">
        <?php foreach ($query as $query_row) { ?>
          <option value="<?php echo $query_row['ID'];?>"><?php echo $query_row['name'];?></option>
        <?php } ?>
    </select>
  <?php } else { ?>
    <input type="input" class="hidden" name="no_open_tickets" id="no_open_tickets" required="required">
    <p class="text-center text-muted">Hurray, no open Tickets!</p>
  <?php } ?>
  <?php $query = table_content('Devs', $conn); ?>
  <?php if ($query) { ?>
    <label for="dev_id">Please select a Dev!</label>
    <select id="dev_id" name="dev_id" class="form-control">
        <?php foreach ($query as $query_row) { ?>
          <option value="<?php echo $query_row['ID'];?>"><?php echo $query_row['name'];?></option>
        <?php } ?>
    </select>
  <?php } else { ?>
    <input type="input" class="hidden" name="no_open_tickets" id="no_open_tickets" required="required">
    <p class="text-center text-muted">Crap, no Dev added yet!</p>
  <?php } ?>
</div>