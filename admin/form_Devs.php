<?php include('../config/functions.php');?>
<input type="hidden" name="addType" id="addType" value="Devs">
<div class="form-group col-xs-6">
  <input type="name" class="form-control input-lg" id="name" name="name" placeholder="Name*" required>
</div>
<div class="form-group col-xs-6">
  <input type="email" class="form-control input-lg" id="email" name="email" placeholder="Email address*" required>
</div>
<div class="form-group col-xs-12">
  <textarea rows="8" class="form-control" name="description" id="description" placeholder="Please write a short introduction about the new Dev!*" required=""></textarea>
</div>
<div class="form-group col-xs-12">
  <label for="role_id">Please select a Role!</label>
  <select id="role_id" name="role_id" class="form-control" required>
    <?php $query = table_content('Roles', $conn); ?>
    <?php if ($query) {?>
      <?php foreach ($query as $query_row) { ?>
        <option value="<?php echo $query_row['ID'];?>"><?php echo $query_row['name'];?></option>
      <?php } ?>
    <?php } ?>
  </select>
</div>