<?php include('../config/functions.php');?>
<input type="hidden" name="addType" id="addType" value="Tickets">
<div class="form-group col-xs-12">
  <input type="name" class="form-control input-lg" id="name" name="name" placeholder="What is wrong?">
</div>
<div class="form-group col-xs-12">
  <textarea rows="8" class="form-control" name="description" id="description" placeholder="Please explain what's gone wrong, what are the symptoms and how can we help you!"></textarea>
</div>
<div class="form-group col-xs-12">
  <?php $query = table_content('Devs', $conn); ?>
  <?php if ($query) {?>
    <label for="dev_id">Please select a Dev to assign this Ticket to!</label>
    <select id="dev_id" name="dev_id" class="form-control">
      <?php foreach ($query as $query_row) { ?>
        <option value="<?php echo $query_row['ID'];?>"><?php echo $query_row['name'];?></option>
      <?php } ?>
    </select>
  <?php } else { ?>
    <input type="input" class="hidden" name="no_devs" id="no_devs" required="required">
    <p class="text-center text-muted">Oops, no Dev is available!</p>
  <?php } ?>
</div>