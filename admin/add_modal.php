<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">
          <i class="icon-close"></i>
        </span>
      </button>
      <h4 class="modal-title">New</h4>
    </div>
    <div class="row">
      <div class="modal-body col-sm-8 col-sm-offset-2" id="modal-body">
        <form method="POST" id="create_form">
          <div id="create-form-contents" class="clearfix">
          </div>
          <p class="text-center">
            <button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-check vcenter padding-right"></i> <span class="vcenter" id="button-content"></span></button>
          </p>
        </form>
      </div>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->