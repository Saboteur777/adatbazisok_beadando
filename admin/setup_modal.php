<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <div class="status"><i class="fa-3x icon-refresh icon-spin"></i></div>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">
          <i class="icon-close"></i>
        </span>
      </button>
      <h4 class="modal-title">New</h4>
    </div>
    <div class="row">
      <div class="modal-body col-sm-8 col-sm-offset-2" id="modal-body">
        <form method="POST" id="setup_form">
          <div id="setup-form-contents" class="clearfix"></div>
        </form>
      </div>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->