<span class="vcenter" data-toggle="tooltip" data-placement="top" title="Delete">
	<button 
		class="btn btn-link pull-left delete-button" 
		data-id="<?php echo $row['ID'];?>" 
		data-type="<?php echo $type;?>"
		>
		<i class="icon-trash"></i>
	</button>
</span>
<span class="vcenter" data-toggle="tooltip" data-placement="top" title="Edit">
	<button 
		class="btn btn-link pull-left edit-button" 
		data-toggle="modal" 
		data-target="#edit" 
		data-id="<?php echo $row['ID'];?>" 
		data-type="<?php echo $type;?>" 
		data-title="<?php echo $row['name'];?>" 
		data-email="<?php echo $row['email'];?>" 
		data-description="<?php echo $row['description'];?>"
		data-role_id="<?php echo $row['role_id'];?>"
		data-dev_id="<?php echo $row['dev_id'];?>"
		data-code="<?php echo $row['code'];?>"
		data-ticket_id="<?php echo $row['ticket_id'];?>"
		>
		<i class="icon-pencil"></i>
	</button>
</span>