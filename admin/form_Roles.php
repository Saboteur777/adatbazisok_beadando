<?php include('../config/functions.php');?>
<input type="hidden" name="addType" id="addType" value="Roles">
<div class="form-group col-xs-12">
  <input type="name" class="form-control input-lg" id="name" name="name" placeholder="Role Name*" required>
</div>
<div class="form-group col-xs-12">
  <textarea rows="8" class="form-control" name="description" id="description" placeholder="Please write a short description about the new Role!*" required></textarea>
</div>