<!DOCTYPE html>
	<?php $page_title = "Released Patches";?>
	<?php $page_subtitle = "We have not failed, we have just found 10,000 ways that do not work.";?>
	<?php $type = "Patches";?>
	<?php include($_SERVER["DOCUMENT_ROOT"].'/templates/head.php');?>
	<body>
		<?php include(get_template_part('templates/nav.php'));?>
		<?php include(get_template_part('templates/header.php'));?>
		<?php include(get_template_part('templates/archive.php'));?>
	</body>
	<?php include(get_template_part('templates/footer.php'));?>
</html>