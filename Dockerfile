FROM merorafael/php-apache:7.1
RUN a2enmod rewrite
RUN chmod +w /var/www/html -R
RUN service apache2 restart