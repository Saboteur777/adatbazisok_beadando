jQuery(document).ready(function($){
	$('#create').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var add_title = button.data('add-title');
		var add_type = button.data('add-type');
		var modal = $(this);
		var id = modal.attr('id');
		var form_body = $('#create-form-contents');
		$.ajax({
			url: '/admin/form_' + add_type + '.php', 
			success: function(data) {
				form_body.html(data);
			}
		})

		// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		modal.find('.modal-title').text(add_title);
		modal.find('#addType').val(add_type);
		modal.find('#button-content').text(add_title);
	});
	$('#setup').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var add_title = button.data('add-title');
		var add_type = button.data('add-type');
		var modal = $(this);
		var id = modal.attr('id');
		var form_body = $('#setup-form-contents');
		$('.status').html('<i class="fa-3x icon-refresh icon-spin"></i>');
		$.ajax({
			url: '/admin/setup_' + add_type + '.php', 
			success: function(data) {
				form_body.html(data);
			}
		})

		// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		modal.find('.modal-title').text(add_title);
	});

	$('#edit').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var id = button.data('id');
		var title = button.data('title');
		var email = button.data('email');
		var description = button.data('description');
		var role_id = button.data('role_id');
		var dev_id = button.data('dev_id');
		var code = button.data('code');
		var ticket_id = button.data('ticket_id');
		var type = button.data('type');
		var modal = $(this);
		var modal_id = modal.attr('id');
		var form_body = $('#edit-form-contents');
		$.ajax({
			url: '/admin/form_' + type + '.php', 
			success: function(data) {
				form_body.html(data);
			}
		})

		modal.find('.modal-title').text('Edit ' + title);
		modal.find('#addType').val(type);
		
		setTimeout(function() {
			modal.find('#ID').val(id);
			modal.find('#name').val(title);
			modal.find('#email').val(email);
			modal.find('#description').val(description);
			modal.find('#role_id').val(role_id);
			modal.find('#dev_id').val(dev_id);
			modal.find('#code').val(code);
			modal.find('#ticket_id').val(ticket_id);
		 }, 200);
	});

	function delete_row(id, type) {
		$.ajax({
			type: "POST",
			url: 'admin/delete.php',
			data: {action: 'delete_row', ID: id, type: type},
			success:function(html) {
				console.log("run");
			}
		});
	}

	$('.delete-button').click(function(event){

		var button = event.currentTarget;
		var id = $(button).data('id');
		var type = $(button).data('type');

	    delete_row(id, type);
	    var gr_parent = $(button).parent().parent().parent();
		$(gr_parent).addClass('animated zoomOut');
		setTimeout(function() {
		     $(gr_parent).remove();
		 }, 500);

	});

	$("[data-toggle='tooltip']").tooltip();

});	