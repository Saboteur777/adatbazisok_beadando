<?php 

	/**
	 * Singleton class
	 *
	 */
	final class accessDatabase {
	    /**
	     * Call this method to get singleton
	     *
	     * @return accessDatabase
	     */
	    public static function Instance()
	    {
	        static $inst = null;
	        if ($inst === null) {
	            $inst = new accessDatabase();
	        }
	        return $inst;
	    }

	    /**
	     * Private constructor so nobody else can instance it
	     *
	     */
	    private function __construct() {
	    }

	    public static function connect_to_db() {
	    	$tns = "
				(DESCRIPTION =
					(ADDRESS_LIST =
						(ADDRESS = (PROTOCOL = TCP)(HOST = oracle)(PORT = 1521))
					)
					(CONNECT_DATA =
						(SID = xe)
					)
				  )
			";

			$db = oci_connect('system', 'oracle', $tns,'UTF8');
			return $db;
	    }
	}

	$conn = accessDatabase::Instance()->connect_to_db();

	function table_schema() {
		$table_schema = array(
			"Devs" => array(
				"type" => "table",
				"content" => "
					CREATE TABLE Devs (
						ID NUMBER(3), 
						name VARCHAR(60) NOT NULL, 
						email VARCHAR(50) NOT NULL, 
						description VARCHAR(1000) NOT NULL, 
						role_id NUMBER(3) DEFAULT 3, 
						CONSTRAINT devs_pk PRIMARY KEY (ID)
					)
				"
			),
			"Roles" => array(
				"type" => "table", 
				"content" => "
					CREATE TABLE Roles (
						ID NUMBER(3), 
						name VARCHAR(60) NOT NULL,
						description VARCHAR(1000) NOT NULL,
						CONSTRAINT roles_pk PRIMARY KEY (ID)
					)
				"
			),
			"Tickets" => array(
				"type" => "table", 
				"content" => "
					CREATE TABLE Tickets (
						ID NUMBER(3), 
						name VARCHAR(60) NOT NULL,
						create_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
						description VARCHAR(1000) NOT NULL,
						dev_id NUMBER(3),
						patch_id NUMBER(3),
						status VARCHAR(30) DEFAULT 'open' NOT NULL,
						CONSTRAINT tickets_pk PRIMARY KEY (ID),
						FOREIGN KEY (dev_id) REFERENCES Devs(ID) ON DELETE CASCADE
					)
				"
			),
			"Patches" => array(
				"type" => "table", 
				"content" => "
					CREATE TABLE Patches (
						ID NUMBER(3), 
						name VARCHAR(60) NOT NULL,
						create_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
						description VARCHAR(1000) NOT NULL,
						code VARCHAR(1000) NOT NULL,
						ticket_id NUMBER(3),
						dev_id NUMBER(3),
						CONSTRAINT patches_pk PRIMARY KEY (ID),
						FOREIGN KEY (dev_id) REFERENCES Devs(ID) ON DELETE CASCADE, 
						FOREIGN KEY (ticket_id) REFERENCES Tickets(ID) ON DELETE CASCADE
					)
				"
			), 
			"DEV_ID_CALC" => array(
				"type" => "sequence",
				"content" => "
					CREATE SEQUENCE DEV_ID_CALC
					  START WITH 1
					  INCREMENT BY 1
					  CACHE 100
				"
			),

			"ROLE_ID_CALC" => array(
				"type" => "sequence",
				"content" => "
					CREATE SEQUENCE ROLE_ID_CALC
					  START WITH 1
					  INCREMENT BY 1
					  CACHE 100
				"
			),

			"TICKET_ID_CALC" => array(
				"type" => "sequence",
				"content" => "
					CREATE SEQUENCE TICKET_ID_CALC
					  START WITH 1
					  INCREMENT BY 1
					  CACHE 100
				"
			),

			"PATCH_ID_CALC" => array(
				"type" => "sequence",
				"content" => "
					CREATE SEQUENCE PATCH_ID_CALC
					  START WITH 1
					  INCREMENT BY 1
					  CACHE 100
				"
			),
			"DEV_ID_SET" => array(
				"type" => "trigger",
				"content" => "
					CREATE OR REPLACE TRIGGER DEV_ID_SET
					  BEFORE INSERT ON Devs
					  FOR EACH ROW
					BEGIN
					  :new.ID := DEV_ID_CALC.nextval;
					END;
				"
			),

			"ROLE_ID_SET" => array(
				"type" => "trigger",
				"content" => "
					CREATE OR REPLACE TRIGGER ROLE_ID_SET
					  BEFORE INSERT ON Roles
					  FOR EACH ROW
					BEGIN
					  :new.ID := ROLE_ID_CALC.nextval;
					END;
				"
			),

			"TICKET_ID_SET" => array(
				"type" => "trigger",
				"content" => "
					CREATE OR REPLACE TRIGGER TICKET_ID_SET
					  BEFORE INSERT ON Tickets
					  FOR EACH ROW
					BEGIN
					  :new.ID := TICKET_ID_CALC.nextval;
					END;
				"
			),

			"PATCH_ID_SET" => array(
				"type" => "trigger",
				"content" => "
					CREATE OR REPLACE TRIGGER PATCH_ID_SET
					  BEFORE INSERT ON Patches
					  FOR EACH ROW
					BEGIN
					  :new.ID := PATCH_ID_CALC.nextval;
					END;
				"
			)
		); 

		return $table_schema;
	}

	// Data generated at http://www.generatedata.com/
	function devs_sample_data() {

		$devs_sample_data = array(
			array('addType' => 'Devs', "name"=>"Radics Ottó","email"=>"otto@webmenedzser.hu","description"=>"Hallgató :)","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Hop O. Carrillo","email"=>"eros.Nam@ridiculusmus.com","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Martina K. Simpson","email"=>"nonummy.Fusce.fermentum@Nullamsuscipit.co.uk","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Randall G. Watson","email"=>"magnis.dis.parturient@risus.ca","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Hilary F. Harvey","email"=>"non.justo@adipiscingelitAliquam.edu","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Grady V. Benjamin","email"=>"Nunc@egestaslaciniaSed.org","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien.","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Tasha H. Sykes","email"=>"tristique.ac.eleifend@maurisrhoncus.com","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Kelsie Y. Robles","email"=>"Praesent.interdum@at.edu","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu,","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Nerea D. Vance","email"=>"habitant@felisorci.net","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Chaim L. Sellers","email"=>"aliquet.lobortis@nuncid.ca","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Ariel D. Cunningham","email"=>"orci.Donec@tincidunt.net","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat.","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Preston I. Walker","email"=>"molestie.sodales@temporbibendum.net","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed,","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Priscilla C. Frank","email"=>"a.purus@Crasinterdum.org","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Victoria X. York","email"=>"ante.Nunc@ullamcorperDuis.ca","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non,","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Samantha Y. Case","email"=>"urna.justo.faucibus@odiotristiquepharetra.org","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed,","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Freya Z. Foreman","email"=>"ultricies@magna.ca","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Ulric Q. Barber","email"=>"montes@quamquis.co.uk","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Imani N. Dickerson","email"=>"hendrerit@arcuAliquam.co.uk","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Declan B. James","email"=>"feugiat@ante.ca","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Vivien U. Wilkins","email"=>"iaculis@malesuadafames.edu","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Thaddeus U. Pace","email"=>"metus@risusNulla.ca","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Lucian E. Sutton","email"=>"Duis.sit@Aliquam.co.uk","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Tanner M. Mays","email"=>"et.nunc@Praesentinterdum.com","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Genevieve T. Wise","email"=>"cursus.vestibulum.Mauris@purusaccumsaninterdum.ca","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Ahmed O. Osborn","email"=>"enim.Sed@sedconsequatauctor.com","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris,","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Rowan M. Moran","email"=>"vitae@dis.edu","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a,","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Hu I. Morrison","email"=>"Cum.sociis@enimgravida.org","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam.","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Amy F. Powers","email"=>"eu.metus.In@vitaeeratvel.net","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Geraldine K. Marks","email"=>"sem.vitae@diamdictumsapien.com","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Nadine G. Lester","email"=>"Cras.eu.tellus@lacusQuisque.co.uk","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Carl Y. Reese","email"=>"non.dui@mattisvelit.org","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae,","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Ross B. Hughes","email"=>"eu.eleifend@necmollis.edu","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Quyn S. Coffey","email"=>"nascetur@sollicitudincommodoipsum.edu","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Melissa K. Wilder","email"=>"commodo@varius.co.uk","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis","role_id"=>2),
			array('addType' => 'Devs', "name"=>"MacKensie P. Serrano","email"=>"ut.sem.Nulla@lectusa.org","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Walker Y. Lester","email"=>"a.dui.Cras@purus.org","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed,","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Candace V. Randall","email"=>"Morbi.non.sapien@iderat.org","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Herman J. Rose","email"=>"sodales.at@congueturpis.org","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Angela K. Alston","email"=>"Aliquam@ipsumdolorsit.net","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor.","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Oprah G. Hobbs","email"=>"at@semutcursus.co.uk","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Ashton O. Clark","email"=>"Nunc@etcommodoat.org","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis.","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Halee P. Conway","email"=>"orci@nequeMorbiquis.org","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Cleo A. Carlson","email"=>"odio@arcuimperdiet.net","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Cullen J. Church","email"=>"malesuada.vel@purussapiengravida.edu","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Noelle O. Mcleod","email"=>"dui@vestibulum.org","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Meghan W. Cameron","email"=>"non.lobortis@ipsumSuspendisse.org","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum.","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Caryn G. Smith","email"=>"mollis.nec.cursus@Cumsociis.ca","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Karyn H. Sweeney","email"=>"dui@Phasellus.co.uk","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Carissa C. Middleton","email"=>"fames.ac@magna.com","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Otto A. Wilkerson","email"=>"diam@augue.co.uk","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Shafira V. Cruz","email"=>"Phasellus.nulla@sed.co.uk","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede.","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Noel A. Kirk","email"=>"sit.amet.dapibus@sit.net","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Luke L. Odonnell","email"=>"lorem.vehicula@nonummyultriciesornare.com","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Inga K. Wheeler","email"=>"ipsum.Suspendisse@lectusquis.com","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris,","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Grady K. Valdez","email"=>"purus@velfaucibusid.ca","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Michael Q. Burns","email"=>"elementum.purus.accumsan@antelectus.com","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis.","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Lyle B. Gonzalez","email"=>"eget@ipsumDonecsollicitudin.org","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Harper Y. Craft","email"=>"sed.sapien.Nunc@mattis.co.uk","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Hammett R. Singleton","email"=>"eu.arcu.Morbi@estvitae.co.uk","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci.","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Amal P. Ross","email"=>"velit@ullamcorpervelit.co.uk","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Kelsie P. Camacho","email"=>"gravida@nonummyFuscefermentum.ca","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Jason S. Dudley","email"=>"dui.Fusce.aliquam@felisorci.net","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien.","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Carissa N. Weeks","email"=>"purus.ac.tellus@lectusjusto.ca","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Xerxes Z. Franks","email"=>"dui@ligula.com","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed,","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Blake N. Zimmerman","email"=>"dolor@duiCumsociis.net","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor.","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Kiona C. Harrell","email"=>"Nulla@leoin.edu","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Geoffrey B. Hurley","email"=>"semper@nulla.org","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Dakota Z. Cochran","email"=>"rutrum@Inmi.ca","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Kirestin F. Barron","email"=>"odio.Phasellus.at@venenatisvelfaucibus.edu","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis.","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Kelly X. Howell","email"=>"ante@antebibendumullamcorper.edu","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Ann R. Ward","email"=>"blandit.congue@enim.co.uk","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Odessa C. Reynolds","email"=>"est.Nunc@lorem.org","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Candice I. Vega","email"=>"Nulla.eget@sedsem.co.uk","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Chanda T. Farmer","email"=>"iaculis.lacus.pede@neque.net","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Flavia F. Hatfield","email"=>"Morbi@Fusce.net","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Gretchen P. Grimes","email"=>"enim.consequat.purus@Cumsociis.ca","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat.","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Yael E. Gay","email"=>"erat@sagittis.org","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Miranda N. Fitzpatrick","email"=>"magnis.dis.parturient@NullamenimSed.net","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Byron E. Price","email"=>"orci.sem.eget@id.co.uk","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Kermit Q. Oneill","email"=>"ipsum.Suspendisse@eleifendvitae.co.uk","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Odessa O. Alexander","email"=>"cursus.et.eros@turpis.org","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Brian L. Wilkins","email"=>"malesuada.fringilla@loremvehiculaet.com","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Zelda M. Vaughan","email"=>"sapien.Nunc@elit.com","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus.","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Quentin A. Hawkins","email"=>"Aenean.gravida@quisdiamluctus.edu","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Brenda B. Bender","email"=>"imperdiet.erat@sit.edu","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Eve W. Everett","email"=>"vel@utmiDuis.com","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Mariam O. Phelps","email"=>"nonummy@massa.ca","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Xandra P. King","email"=>"cursus@Suspendissealiquetsem.co.uk","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam.","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Octavia F. House","email"=>"ipsum.ac.mi@diamProindolor.org","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Uriel U. Howe","email"=>"ac.urna@diamvelarcu.net","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Jonas K. Foley","email"=>"nunc.Quisque.ornare@sapien.co.uk","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Bevis B. Johnston","email"=>"laoreet.ipsum@ullamcorperDuis.ca","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae,","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Kenyon A. Odom","email"=>"vitae.sodales.nisi@enimmitempor.org","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id,","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Cullen W. Guerrero","email"=>"ac.turpis.egestas@Nullamfeugiatplacerat.edu","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Ori G. Rollins","email"=>"euismod.ac.fermentum@duiFusce.com","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus","role_id"=>3),
			array('addType' => 'Devs', "name"=>"Amena W. Wiggins","email"=>"tempor.arcu@vitaesodalesnisi.com","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Ian Q. Roberson","email"=>"dictum.sapien@in.ca","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Janna J. Franks","email"=>"Donec.nibh@maurissagittis.net","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus","role_id"=>1),
			array('addType' => 'Devs', "name"=>"Ahmed B. Vazquez","email"=>"In.scelerisque.scelerisque@loremeget.edu","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam,","role_id"=>2),
			array('addType' => 'Devs', "name"=>"Isaac W. Hurley","email"=>"felis.eget.varius@erosnonenim.co.uk","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id,","role_id"=>2)
		);

		return $devs_sample_data;
	}

	function roles_sample_data() {

		$roles_sample_data = array(
			array('addType' => 'Roles', 'name' => 'Admin', 'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. '),
			array('addType' => 'Roles', 'name' => 'Contributor', 'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. '),
			array('addType' => 'Roles', 'name' => 'User', 'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ')
		);

		return $roles_sample_data;
	}

	function tickets_sample_data() {

		$tickets_sample_data = array(
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum","dev_id"=>22),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin","dev_id"=>60),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id,","dev_id"=>52),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna.","dev_id"=>52),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus","dev_id"=>33),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna.","dev_id"=>10),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum","dev_id"=>46),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu","dev_id"=>7),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque","dev_id"=>33),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non,","dev_id"=>53),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin","dev_id"=>5),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida","dev_id"=>20),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper.","dev_id"=>54),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada","dev_id"=>14),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque","dev_id"=>13),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam","dev_id"=>13),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id,","dev_id"=>14),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at","dev_id"=>31),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus","dev_id"=>38),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa","dev_id"=>12),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a,","dev_id"=>37),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus.","dev_id"=>37),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum","dev_id"=>65),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque","dev_id"=>46),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna.","dev_id"=>54),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus.","dev_id"=>44),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada","dev_id"=>1),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa","dev_id"=>26),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin","dev_id"=>21),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum","dev_id"=>58),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id,","dev_id"=>65),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna.","dev_id"=>24),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin","dev_id"=>53),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam","dev_id"=>60),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus.","dev_id"=>66),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis","dev_id"=>36),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida","dev_id"=>17),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum","dev_id"=>22),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non,","dev_id"=>12),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id,","dev_id"=>62),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada","dev_id"=>24),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat.","dev_id"=>26),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non,","dev_id"=>53),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus","dev_id"=>40),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin","dev_id"=>24),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id,","dev_id"=>51),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum","dev_id"=>20),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada","dev_id"=>14),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at","dev_id"=>61),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum","dev_id"=>52),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa","dev_id"=>65),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus","dev_id"=>33),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at","dev_id"=>17),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet","dev_id"=>41),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu","dev_id"=>19),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus","dev_id"=>28),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis","dev_id"=>33),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis","dev_id"=>20),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus","dev_id"=>27),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus.","dev_id"=>32),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida","dev_id"=>60),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida","dev_id"=>12),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper.","dev_id"=>36),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus.","dev_id"=>20),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum","dev_id"=>64),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin","dev_id"=>18),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper.","dev_id"=>22),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus.","dev_id"=>14),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non,","dev_id"=>1),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna.","dev_id"=>36),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet","dev_id"=>32),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet","dev_id"=>64),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id,","dev_id"=>17),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa","dev_id"=>61),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa","dev_id"=>46),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum","dev_id"=>28),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida","dev_id"=>50),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a,","dev_id"=>30),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa","dev_id"=>55),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat.","dev_id"=>56),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus.","dev_id"=>8),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin","dev_id"=>39),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet","dev_id"=>4),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum","dev_id"=>44),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat.","dev_id"=>56),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet","dev_id"=>56),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus.","dev_id"=>55),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu","dev_id"=>35),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna.","dev_id"=>57),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum","dev_id"=>16),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id,","dev_id"=>21),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna.","dev_id"=>34),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis","dev_id"=>50),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis","dev_id"=>41),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis","dev_id"=>54),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien,","dev_id"=>62),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque","dev_id"=>23),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis","dev_id"=>10),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida","dev_id"=>25),
			array('addType' => 'Tickets', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet","dev_id"=>62)
		);

		return $tickets_sample_data;
	}

	function patches_sample_data() {

		$patches_sample_data = array(
			array('addType' => 'Patches', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida","code"=>"sed leo. Cras vehicula","ticket_id"=>1,"dev_id"=>39),
			array('addType' => 'Patches', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet","code"=>"enim, gravida sit amet,","ticket_id"=>2,"dev_id"=>29),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa","code"=>"a mi fringilla mi","ticket_id"=>3,"dev_id"=>49),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada","code"=>"purus ac tellus. Suspendisse","ticket_id"=>4,"dev_id"=>12),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus.","code"=>"a, magna. Lorem ipsum","ticket_id"=>5,"dev_id"=>33),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat.","code"=>"Aenean sed pede nec","ticket_id"=>6,"dev_id"=>32),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam","code"=>"luctus et ultrices posuere","ticket_id"=>7,"dev_id"=>23),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at","code"=>"Aliquam auctor, velit eget","ticket_id"=>8,"dev_id"=>34),
			array('addType' => 'Patches', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum","code"=>"lectus convallis est, vitae","ticket_id"=>9,"dev_id"=>26),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus","code"=>"fames ac turpis egestas.","ticket_id"=>10,"dev_id"=>50),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida","code"=>"tellus. Nunc lectus pede,","ticket_id"=>11,"dev_id"=>4),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum","code"=>"fermentum metus. Aenean sed","ticket_id"=>12,"dev_id"=>48),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper.","code"=>"lobortis augue scelerisque mollis.","ticket_id"=>13,"dev_id"=>59),
			array('addType' => 'Patches', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna.","code"=>"eu sem. Pellentesque ut","ticket_id"=>14,"dev_id"=>31),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque","code"=>"vehicula et, rutrum eu,","ticket_id"=>15,"dev_id"=>64),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper.","code"=>"Fusce aliquam, enim nec","ticket_id"=>16,"dev_id"=>25),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque","code"=>"erat eget ipsum. Suspendisse","ticket_id"=>17,"dev_id"=>58),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada","code"=>"Vestibulum accumsan neque et","ticket_id"=>18,"dev_id"=>25),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper.","code"=>"ornare sagittis felis. Donec","ticket_id"=>19,"dev_id"=>17),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna.","code"=>"Curae; Phasellus ornare. Fusce","ticket_id"=>20,"dev_id"=>41),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id,","code"=>"ornare tortor at risus.","ticket_id"=>21,"dev_id"=>54),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa","code"=>"urna suscipit nonummy. Fusce","ticket_id"=>22,"dev_id"=>51),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna.","code"=>"Nullam velit dui, semper","ticket_id"=>23,"dev_id"=>19),
			array('addType' => 'Patches', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam","code"=>"egestas nunc sed libero.","ticket_id"=>24,"dev_id"=>21),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a,","code"=>"nisi a odio semper","ticket_id"=>25,"dev_id"=>44),
			array('addType' => 'Patches', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu","code"=>"aliquam arcu. Aliquam ultrices","ticket_id"=>26,"dev_id"=>20),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa","code"=>"molestie pharetra nibh. Aliquam","ticket_id"=>27,"dev_id"=>39),
			array('addType' => 'Patches', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam","code"=>"neque tellus, imperdiet non,","ticket_id"=>28,"dev_id"=>59),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu","code"=>"arcu iaculis enim, sit","ticket_id"=>29,"dev_id"=>25),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis","code"=>"cursus non, egestas a,","ticket_id"=>30,"dev_id"=>46),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida","code"=>"nunc ac mattis ornare,","ticket_id"=>31,"dev_id"=>11),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper.","code"=>"odio tristique pharetra. Quisque","ticket_id"=>32,"dev_id"=>12),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque","code"=>"sed dolor. Fusce mi","ticket_id"=>33,"dev_id"=>7),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat.","code"=>"Fusce fermentum fermentum arcu.","ticket_id"=>34,"dev_id"=>3),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat.","code"=>"at, velit. Cras lorem","ticket_id"=>35,"dev_id"=>6),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet","code"=>"dolor dolor, tempus non,","ticket_id"=>36,"dev_id"=>45),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna.","code"=>"imperdiet nec, leo. Morbi","ticket_id"=>37,"dev_id"=>3),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at","code"=>"eget, venenatis a, magna.","ticket_id"=>38,"dev_id"=>29),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien,","code"=>"sociosqu ad litora torquent","ticket_id"=>39,"dev_id"=>4),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a,","code"=>"et, eros. Proin ultrices.","ticket_id"=>40,"dev_id"=>48),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis","code"=>"Nunc quis arcu vel","ticket_id"=>41,"dev_id"=>37),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis","code"=>"nibh sit amet orci.","ticket_id"=>42,"dev_id"=>59),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id,","code"=>"hendrerit consectetuer, cursus et,","ticket_id"=>43,"dev_id"=>27),
			array('addType' => 'Patches', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa","code"=>"pretium aliquet, metus urna","ticket_id"=>44,"dev_id"=>2),
			array('addType' => 'Patches', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum","code"=>"enim diam vel arcu.","ticket_id"=>45,"dev_id"=>48),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus","code"=>"consectetuer mauris id sapien.","ticket_id"=>46,"dev_id"=>22),
			array('addType' => 'Patches', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu","code"=>"a, auctor non, feugiat","ticket_id"=>47,"dev_id"=>56),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum","code"=>"sit amet metus. Aliquam","ticket_id"=>48,"dev_id"=>10),
			array('addType' => 'Patches', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien,","code"=>"Suspendisse eleifend. Cras sed","ticket_id"=>49,"dev_id"=>60),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada","code"=>"facilisis, magna tellus faucibus","ticket_id"=>50,"dev_id"=>51),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam","code"=>"eu lacus. Quisque imperdiet,","ticket_id"=>51,"dev_id"=>6),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet","code"=>"laoreet posuere, enim nisl","ticket_id"=>52,"dev_id"=>6),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa","code"=>"sed sem egestas blandit.","ticket_id"=>53,"dev_id"=>30),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada","code"=>"Nulla facilisi. Sed neque.","ticket_id"=>54,"dev_id"=>45),
			array('addType' => 'Patches', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa","code"=>"Nunc laoreet lectus quis","ticket_id"=>55,"dev_id"=>9),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus.","code"=>"Fusce fermentum fermentum arcu.","ticket_id"=>56,"dev_id"=>52),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam","code"=>"felis ullamcorper viverra. Maecenas","ticket_id"=>57,"dev_id"=>37),
			array('addType' => 'Patches', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa","code"=>"amet massa. Quisque porttitor","ticket_id"=>58,"dev_id"=>27),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper.","code"=>"venenatis lacus. Etiam bibendum","ticket_id"=>59,"dev_id"=>19),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus","code"=>"nec orci. Donec nibh.","ticket_id"=>60,"dev_id"=>63),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non,","code"=>"fames ac turpis egestas.","ticket_id"=>61,"dev_id"=>37),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada","code"=>"Nam ligula elit, pretium","ticket_id"=>62,"dev_id"=>3),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam","code"=>"dictum placerat, augue. Sed","ticket_id"=>63,"dev_id"=>54),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida","code"=>"nec orci. Donec nibh.","ticket_id"=>64,"dev_id"=>49),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien,","code"=>"gravida nunc sed pede.","ticket_id"=>65,"dev_id"=>29),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at","code"=>"tincidunt congue turpis. In","ticket_id"=>66,"dev_id"=>57),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a,","code"=>"ullamcorper viverra. Maecenas iaculis","ticket_id"=>67,"dev_id"=>13),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien,","code"=>"congue. In scelerisque scelerisque","ticket_id"=>68,"dev_id"=>54),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna.","code"=>"sapien, gravida non, sollicitudin","ticket_id"=>69,"dev_id"=>43),
			array('addType' => 'Patches', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien,","code"=>"ornare, elit elit fermentum","ticket_id"=>70,"dev_id"=>25),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non,","code"=>"Duis sit amet diam","ticket_id"=>71,"dev_id"=>44),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus.","code"=>"et malesuada fames ac","ticket_id"=>72,"dev_id"=>55),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus.","code"=>"erat neque non quam.","ticket_id"=>73,"dev_id"=>5),
			array('addType' => 'Patches', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus.","code"=>"risus a ultricies adipiscing,","ticket_id"=>74,"dev_id"=>65),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus.","code"=>"neque vitae semper egestas,","ticket_id"=>75,"dev_id"=>64),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna.","code"=>"nec tellus. Nunc lectus","ticket_id"=>76,"dev_id"=>29),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat.","code"=>"quis accumsan convallis, ante","ticket_id"=>77,"dev_id"=>45),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id,","code"=>"metus. In lorem. Donec","ticket_id"=>78,"dev_id"=>36),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque","code"=>"cursus purus. Nullam scelerisque","ticket_id"=>79,"dev_id"=>17),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non,","code"=>"a feugiat tellus lorem","ticket_id"=>80,"dev_id"=>45),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque","code"=>"Morbi sit amet massa.","ticket_id"=>81,"dev_id"=>55),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a,","code"=>"placerat eget, venenatis a,","ticket_id"=>82,"dev_id"=>32),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu","code"=>"netus et malesuada fames","ticket_id"=>83,"dev_id"=>34),
			array('addType' => 'Patches', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at","code"=>"egestas. Aliquam nec enim.","ticket_id"=>84,"dev_id"=>19),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat.","code"=>"auctor vitae, aliquet nec,","ticket_id"=>85,"dev_id"=>63),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at","code"=>"mauris. Suspendisse aliquet molestie","ticket_id"=>86,"dev_id"=>8),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a,","code"=>"ipsum dolor sit amet,","ticket_id"=>87,"dev_id"=>32),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien,","code"=>"libero et tristique pellentesque,","ticket_id"=>88,"dev_id"=>3),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus","code"=>"massa. Vestibulum accumsan neque","ticket_id"=>89,"dev_id"=>8),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque","code"=>"ornare sagittis felis. Donec","ticket_id"=>90,"dev_id"=>5),
			array('addType' => 'Patches', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien,","code"=>"nec urna et arcu","ticket_id"=>91,"dev_id"=>24),
			array('addType' => 'Patches', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at","code"=>"ornare, lectus ante dictum","ticket_id"=>92,"dev_id"=>35),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa","code"=>"In tincidunt congue turpis.","ticket_id"=>93,"dev_id"=>48),
			array('addType' => 'Patches', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida","code"=>"ac, fermentum vel, mauris.","ticket_id"=>94,"dev_id"=>17),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien,","code"=>"Morbi non sapien molestie","ticket_id"=>95,"dev_id"=>18),
			array('addType' => 'Patches', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id,","code"=>"rutrum eu, ultrices sit","ticket_id"=>96,"dev_id"=>66),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin","code"=>"sociis natoque penatibus et","ticket_id"=>97,"dev_id"=>50),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper.","code"=>"adipiscing fringilla, porttitor vulputate,","ticket_id"=>98,"dev_id"=>46),
			array('addType' => 'Patches', "name"=>"Lorem ipsum dolor sit amet,","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa","code"=>"cursus, diam at pretium","ticket_id"=>99,"dev_id"=>52),
			array('addType' => 'Patches', "name"=>"Lorem ipsum","description"=>"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida","code"=>"magna. Suspendisse tristique neque","ticket_id"=>100,"dev_id"=>7)
		);

		return $patches_sample_data;
	}

	function table_sample_fill($table_sample_data, $conn) {

		foreach ($table_sample_data as $sample_data) {
			add_to_db($sample_data, $conn);
			$type = $sample_data['addType'];
		}

		echo "<div class='alert alert-success'><strong>" . $type . " sample data</strong> added successfully!</div>";

	}

	function installed($table_schema, $conn) {

		// FOREIGN KEY ÉS REFERENCES ILLETVE ON DELETE SET NULL!!!
		$return = 0;

		foreach ($table_schema as $key => $sql) {
			if (oci_execute(oci_parse($conn, $sql['content']))) {
				echo "<div class='alert alert-success'>" . $key . " created successfully!</div>";
			}
		}

		table_sample_fill(devs_sample_data(), $conn);
		table_sample_fill(roles_sample_data(), $conn);
		table_sample_fill(tickets_sample_data(), $conn);
		table_sample_fill(patches_sample_data(), $conn);

		$check = '<i class="fa-3x icon-check"></i>';
		echo "<script>$('.status').html('" . $check . "');</script>";

		//return $return;

	}

	function uninstalled($table_schema, $conn) {

		$return = 0;
		
		foreach (array_reverse($table_schema) as $key => $sql) {
			if (oci_execute(oci_parse($conn, "DROP " . $sql["type"] . " " . $key))) {
				echo "<div class='alert alert-success'>" . $key . " dropped successfully!</div>";
			} else {
				echo "<div class='alert alert-danger'><strong>Error</strong> on deleting " . $key . "!</div>";
			}
		}

		$check = '<i class="fa-3x icon-check"></i>';
		echo "<script>$('.status').html('" . $check . "');</script>";
		
	}

	function table_content($type, $conn) {
		
		$sql = "SELECT * FROM " . $type;

		$stid = oci_parse( $conn, $sql );
		oci_execute($stid);
		
		$rows = array();
		
		while (($row = oci_fetch_array($stid, OCI_ASSOC + OCI_RETURN_NULLS)) != false) {
		    $data = array(
				'ID' => $row['ID'],
				'name' => $row['NAME'],
				'create_date' => $row['create_date'],
				'description' => $row['DESCRIPTION'],
				'role_id' => $row['ROLE_ID'],
				'patch_id' => $row['PATCH_ID'],
				'email' =>$row['EMAIL'],
				'code' =>$row['CODE'],
				'ticket_id' => $row['TICKET_ID'],
				'dev_id' => $row['DEV_ID']
			);
			array_push($rows, $data);
		}

		return $rows;

	}

	function get_by_ID($ID, $type, $conn) {

		$sql = "SELECT * FROM " . $type . " WHERE ID=" . $ID;

		$stid = oci_parse( $conn, $sql );
		oci_execute($stid);
		
		$rows = array();
		
		while (($row = oci_fetch_array($stid, OCI_ASSOC + OCI_RETURN_NULLS)) != false) {
		    $data = array(
				'ID' => $row['ID'],
				'name' => $row['name'],
				'create_date' => $row['create_date'],
				'description' => $row['description'],
				'role_id' => $row['role_id'],
				'patch_id' => $row['patch_id'],
				'email' =>$row['email'],
				'code' =>$row['code'],
				'ticket_id' => $row['ticket_id'],
				'dev_id' => $row['dev_id']
			);
			array_push($rows, $data);
		}

		return $rows;

	}

	function add_to_db($array, $conn) {

		if ($array['addType'] === 'Devs') {
			$columns = "name, email, description, role_id";
			$values = "'$array[name]', '$array[email]', '$array[description]', '$array[role_id]'";
		} else if ($array['addType'] === 'Roles') {
			$columns = "name, description";
			$values = "'$array[name]', '$array[description]'";
		} else if ($array['addType'] === 'Tickets') {
			$columns = "name, description, dev_id";
			$values = "'$array[name]', '$array[description]', '$array[dev_id]'";
		} else if ($array['addType'] === 'Patches') {
			$columns = "name, description, code, ticket_id,  dev_id";
			$values = "'$array[name]', '$array[description]', '$array[code]', '$array[ticket_id]', '$array[dev_id]'";
		}

		$sql = "INSERT INTO " . $array['addType'] . " (" . $columns . ") VALUES (" . $values . ")";
		// var_dump($sql);
		oci_execute(oci_parse($conn, $sql));

		return TRUE;

	}

	function update_row($array, $conn) {

		$type = $array['addType'];
		$sql = "UPDATE " . $type . " SET ";
		$count = count($array);
		$i = 1;
		unset($array['addType']);
		foreach ($array as $key => $value) {
			$i++;
			$sql .= "" . $key . " = '" . $value . "'";
			if ($i < $count) {
				$sql .= ", ";
			}
		}
		$sql .= " WHERE " . $type . ".ID = '" . $array['ID'] . "'";

		oci_execute(oci_parse($conn, $sql));

		header("Location: /" . strtolower($type));

	}

	function get_gravatar( $email, $s = 240, $d = 'mm', $r = 'g', $img = false, $atts = array() ) {
		$url = 'https://www.gravatar.com/avatar/';
		$url .= md5( strtolower( trim( $email ) ) );
		$url .= "?s=$s&d=$d&r=$r";
		if ( $img ) {
			$url = '<img src="' . $url . '"';
			foreach ( $atts as $key => $val )
				$url .= ' ' . $key . '="' . $val . '"';
				$url .= ' />';
			}
		return $url;
	}

	function get_template_part($path) {
		$path = $_SERVER["DOCUMENT_ROOT"] . '/' . $path;
		return $path;
	}

	function delete_row($ID, $type, $conn) {
		
		$sql = "DELETE FROM " . $type . " WHERE " . $type . ".ID = " . $ID;

		$stid = oci_parse( $conn, $sql );
		oci_execute($stid);

	}

	function get_devs_with_role($conn) {

		// 2 tábla összekapcsolása JOIN-nal
		
		$sql = "
			SELECT DISTINCT 
				Devs.ID 				AS		ID, 
				Devs.name 				AS		dev_name, 
				Devs.email 				AS		email, 
				Devs.description 		AS		description, 
				Roles.name 				AS		role_name 
			FROM 
				Devs 
					JOIN 
				Roles 
				ON Devs.role_id=Roles.ID 
			ORDER BY Devs.ID
		";

		$stid = oci_parse( $conn, $sql );
		oci_execute($stid);
		
		$rows = array();
		
		while (($row = oci_fetch_array($stid, OCI_ASSOC + OCI_RETURN_NULLS)) != false) {
		    $data = array(
				'ID' => $row['ID'],
				'name' => $row['DEV_NAME'],
				'email' =>$row['EMAIL'],
				'description' => $row['DESCRIPTION'],
				'role_name' => $row['ROLE_NAME'],
			);
			array_push($rows, $data);
		}

		return $rows;
	}

	function get_patches_with_tickets($conn) {

		// 2 tábla összekapcsolása JOIN-nal

		$sql = "
			SELECT 
				Patches.ID 													AS 			patch_id, 
				Patches.name 												AS 			patch_name, 
				TO_CHAR(Patches.create_date, 'YYYY. MM. DD. HH24:MI:SS')	AS 			patch_date, 
				Patches.description 										AS 			patch_description, 
				Patches.code 												AS 			patch_code, 
				Patches.dev_id 												AS 			dev_id, 
				Tickets.ID 													AS 			ticket_id, 
				TO_CHAR(Tickets.create_date, 'YYYY. MM. DD. HH24:MI:SS')	AS 			ticket_date, 
				Tickets.name 												AS 			ticket_name, 
				Tickets.description 										AS 			ticket_description 
			FROM 
				Patches 
					JOIN  
				Tickets 
				ON Patches.ticket_id=Tickets.ID
		";

		$stid = oci_parse( $conn, $sql );
		oci_execute($stid);

		$rows = array();
		
		while (($row = oci_fetch_array($stid, OCI_ASSOC + OCI_RETURN_NULLS)) != false) {
		    $data = array(
				'ID' => $row['PATCH_ID'],
				'patch_name' => $row['PATCH_NAME'],
				'name' => $row['PATCH_NAME'],
				'patch_date' => $row['PATCH_DATE'],
				'patch_description' => $row['PATCH_DESCRIPTION'],
				'description' => $row['PATCH_DESCRIPTION'],
				'patch_code' => $row['PATCH_CODE'],
				'code' => $row['PATCH_CODE'],
				'dev_id' => $row['DEV_ID'],
				'ticket_id' => $row['TICKET_ID'],
				'ticket_date' => $row['TICKET_DATE'],
				'ticket_name' => $row['TICKET_NAME'],
				'ticket_description' => $row['TICKET_DESCRIPTION'],
			);
			array_push($rows, $data);
		}

		return $rows;

	}

?>