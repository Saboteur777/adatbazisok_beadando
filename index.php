<!DOCTYPE html>
	<?php $page_title = "Bugzilla";?>
	<?php include($_SERVER["DOCUMENT_ROOT"].'/templates/head.php');?>
	<body>
		<?php include(get_template_part('templates/nav.php'));?>
		<?php include(get_template_part('templates/header.php'));?>
		<?php include(get_template_part('templates/main.php'));?>
	</body>
	<?php include(get_template_part('templates/footer.php'));?>
</html>