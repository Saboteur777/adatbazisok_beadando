<!DOCTYPE html>
	<?php $page_title = "Administration";?>
	<?php $page_subtitle = "With great power comes great responsibility";?>
	<?php include($_SERVER["DOCUMENT_ROOT"].'/templates/head.php');?>
	<body>
		<?php include(get_template_part('templates/nav.php'));?>
		<?php include(get_template_part('templates/header.php'));?>
		<div class="container">
			<?php if ($_POST) { ?>
				<div class="row nopadding">
					<div class="col-xs-12 col-sm-8 col-sm-offset-2 margin-top">
						<?php if (add_to_db($_POST, $conn) === TRUE) { ?>
							<div class="alert alert-success">
								<h4><?php echo $_POST['name'];?> added to <?php echo $_POST['addType'];?> successfully!</h4>
							</div>
						<?php } else { ?>
							<div class="alert alert-danger">
								<h4>Adding <?php echo $_POST['name'];?> to <?php echo $_POST['addType'];?> failed!</h4>
							</div>
						<?php } ?>
					</div>
				</div>
				<?php unset($_POST);?>
			<?php } ?>
			<div class="row">
				<div class="modal fade" tabindex="-1" role="dialog" id="create">
					<?php include("admin/add_modal.php");?>
				</div>
				<div type="button" tabindex="-1" data-toggle="modal" data-target="#create" class="admin-card col-sm-3 text-center" data-add-title="Add new Role" data-add-type="Roles">
					<div class="card" data-mh="admin-card">
						<i class="fa-4x icon-organization padding block"></i>
						<h3>Add new Role</h3>
					</div>
				</div>
				<div type="button" tabindex="-1" data-toggle="modal" data-target="#create" class="admin-card col-sm-3 text-center" data-add-title="Make new Dev" data-add-type="Devs">
					<div class="card" data-mh="admin-card">
						<i class="fa-4x icon-user-follow padding block"></i>
						<h3>Make new Dev</h3>
					</div>
				</div>
				<div type="button" tabindex="-1" data-toggle="modal" data-target="#create" class="admin-card col-sm-3 text-center" data-add-title="Create new Ticket" data-add-type="Tickets">
					<div class="card" data-mh="admin-card">
						<i class="fa-4x icon-support padding block"></i>
						<h3>Create new Ticket</h3>
					</div>
				</div>
				<div type="button" tabindex="-1" data-toggle="modal" data-target="#create" class="admin-card col-sm-3 text-center" data-add-title="Submit Patch" data-add-type="Patches">
					<div class="card" data-mh="admin-card">
						<i class="fa-4x icon-check padding block"></i>
						<h3>Submit Patch</h3>
					</div>
				</div>
			</div>
			<div class="row text-center">
				<div class="modal fade" tabindex="-1" role="dialog" id="setup">
					<?php include("admin/setup_modal.php");?>
				</div>
				<div type="button" tabindex="-1" data-toggle="modal" data-target="#setup" class="admin-card col-sm-5 vcenter text-center" data-add-type="install" data-add-title="Create Demo Database">
					<div class="card bg-success padding text-left" data-mh="admin-card">
						<span class="vcenter padding"><i class="fa-4x icon-layers"></i></span>
						<h4 class="vcenter">
							Create Demo Database<br/>
							<small>Create the necessary tables and fill it with sample data</small>
						</h4>
					</div>
				</div>
				<div type="button" tabindex="-1" data-toggle="modal" data-target="#setup" class="admin-card col-sm-5 vcenter text-center" data-add-type="delete" data-add-title="Reset Database">
					<div class="card bg-danger padding text-left" data-mh="admin-card">
						<span class="vcenter padding"><i class="fa-4x icon-reload"></i></span>
						<h4 class="vcenter">
							Reset Database<br/>
							<small>Drops all tables</small>
						</h4>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid text-center bg-gray-lighter">
			<div class="row">
				<div class="col-xs-12">
					<h2>Need in help?</h2>
					<p class="lead">Shout out to us!</p>
				</div>
			</div>
		</div>
	</body>
	<?php include(get_template_part('templates/footer.php'));?>
</html>