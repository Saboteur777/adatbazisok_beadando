<!DOCTYPE html>
	<?php $page_title = "Roles";?>
	<?php $page_subtitle = "Check out how we can help you!";?>
	<?php $type = "Roles";?>
	<?php include($_SERVER["DOCUMENT_ROOT"].'/templates/head.php');?>
	<body>
		<?php include(get_template_part('templates/nav.php'));?>
		<?php include(get_template_part('templates/header.php'));?>
		<?php include(get_template_part('templates/archive.php'));?>
	</body>
	<?php include(get_template_part('templates/footer.php'));?>
</html>