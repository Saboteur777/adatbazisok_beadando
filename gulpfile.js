var gulp = require('gulp'),
    del = require('del'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    autoprefixer = require('gulp-autoprefixer'),
    cache = require('gulp-cached'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    imageop = require('gulp-image-optimization');
    rename = require('gulp-rename'),
    cleancss = require('gulp-clean-css')
;

gulp.task('del-images', function () {
    return del([
        'assets/images/*',
    ]);
});

gulp.task('del-css', function () {
    return del([
        'assets/css/*',
    ]);
});

gulp.task('del-js', function () {
    return del([
        'assets/js/*',
    ]);
});

gulp.task('del-fonts', function () {
    return del([
        'assets/fonts/*',
    ]);
});

gulp.task('css', ['del-css'], function () {
    return gulp.src('src/styles/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer('last 4 version'))
    .pipe(cleancss())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('assets/css'))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('images', function(cb) {
    gulp.src(['src/images/**/*.png','src/images/**/*.jpg','src/images/**/*.gif','src/images/**/*.jpeg', 'src/images/**/*.svg'])
    .pipe(cache('image_cache'))
    .pipe(imageop({
        optimizationLevel: 9,
        progressive: true,
        interlaced: true
    }))
    .pipe(gulp.dest('assets/images/')).on('end', cb).on('error', cb);
});

gulp.task('jquery', ['del-js'], function() {
    return gulp.src([
        'bower_components/jquery/dist/jquery.min.js',
        ])
    .pipe(gulp.dest('assets/js'))
});

gulp.task('js', ['jquery'], function() {
    return gulp.src([
        'bower_components/bootstrap-sass/assets/javascripts/bootstrap/modal.js',
        'bower_components/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js',
        'bower_components/bootstrap-sass/assets/javascripts/bootstrap/popover.js',
        'bower_components/matchHeight/dist/jquery.matchHeight-min.js',
        'src/js/init.js'
        ])
    .pipe(concat('scripts.js'))
    .pipe(uglify())
    .pipe(gulp.dest('assets/js'))
});

gulp.task('fonts', ['del-fonts'], function() {
    return gulp.src([
        'bower_components/font-awesome/fonts/*.*', 
        'bower_components/simple-line-icons/fonts/*.*', 
        'src/fonts/*.*'
        ])
    .pipe(gulp.dest('assets/fonts'))
});

gulp.task('browser-sync', function() {
    browserSync.init(null, {
        proxy: 'localhost:3000', //our PHP server
        open: true,
        watchTask: true
    });
});

gulp.task('bs-reload', function () {
    browserSync.reload();
});

gulp.task('default', ['css', 'js', 'fonts', 'images', 'browser-sync'], function () {
    gulp.watch("**/*.php", ['bs-reload']);
    gulp.watch("src/styles/*.scss", ['css']);
    gulp.watch("src/images/*.*", ['images']);
    gulp.watch("src/js/**/*.*", ['js']);
    gulp.watch("*.html", ['bs-reload']);
});