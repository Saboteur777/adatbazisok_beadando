<?php require_once($_SERVER["DOCUMENT_ROOT"].'/config/functions.php');?>

<head>
	<script src="/assets/js/jquery.min.js"></script>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title><?php echo $page_title;?></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<!-- Place favicon.ico in the root directory -->

	<link rel="stylesheet" href="/assets/css/style.min.css">
</head>