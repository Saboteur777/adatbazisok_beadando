<div class="container">
	<?php 
		include(get_template_part('admin/edit_modal.php'));
		if ($type == 'Devs') {
			include(get_template_part('templates/archive_Devs.php'));
		} else if ($type === 'Roles') {
			include(get_template_part('templates/archive_Roles.php'));
		} else if ($type === 'Tickets') {
			include(get_template_part('templates/archive_Tickets.php'));
		} else if ($type === 'Patches') {
			include(get_template_part('templates/archive_Patches.php'));
		} else { 
			include(get_template_part('templates/no_content.php')); 
		} 
	?>
</div>