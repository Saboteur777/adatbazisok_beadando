<?php $rows = get_devs_with_role($conn); ?>
<?php 
	// N+1 probléma
	//$rows = table_content($type); 
?>
<?php if ($rows) { ?>
	<?php foreach ($rows as $row) { 
		if ($row['email']) { 
			$class = "col-xs-8 col-xs-offset-1 col-sm-7 col-sm-offset-0 vcenter";
		} else {
			$class = "col-xs-8 col-xs-offset-2";
		} 
		?>
		<div class="row">
			<div class="<?php echo $class;?>">
				<?php include(get_template_part('admin/edit_menu.php'));?>
				<div class="padding-top-4x padding-bottom-4x">
					<h2>
						<?php echo $row['name'];?><br/>
						<small>
							<?php 
								if ($row['role_name']) {
									echo $row['role_name'] . ",";
								}
							?>
							<?php 
								if ($row['email']) {
									echo $row['email'];
								}
							?>
						</small>
					</h2>
					<?php if ($row['description']) { ?>
						<p class="lead">
							<?php echo $row['description'];?>
						</p>
					<?php } ?>

					<?php 

						// Az alábbi, rossz megoldás N+1 problémát generálna

					?>
					<?php 
						/*
						$related_patches = get_by_id($row['ID'], 'Patches'); 
						if ($related_patches) { ?>

							<table class="table table-striped table-bordered">
								<?php foreach ($related_patches as $patch) { ?>
									<tr>
										<td><?php echo $patch['name'];?></td>
										<td><?php echo $patch['date'];?></td>
									</tr>
								<?php } ?>
							</table>

						<?php } 
						*/
					?>
				</div>
			</div>
			<?php if ($row['email']) { ?>
				<div class="col-xs-6 col-xs-offset-3 col-sm-4 col-sm-offset-0 text-right vcenter">
					<img class="img-thumbnail img-circle" src="<?php echo get_gravatar($row['email']);?>">
				</div>
			<?php } ?>
		</div>
	<?php } ?>
<?php } ?>