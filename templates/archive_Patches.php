<?php $rows = get_patches_with_tickets($conn); ?>
<?php if ($rows) { ?>
	<?php foreach ($rows as $row) { ?>
		<div class="row">
			<div class="col-xs-8 col-xs-offset-2">
				<?php include(get_template_part('admin/edit_menu.php'));?>
				<div class="padding-top-4x padding-bottom-4x">
					<h2>
						<?php echo $row['patch_name'];?><br/>
						<small>
							<?php echo $row['patch_date'];?>
						</small>
					</h2>
					<?php if ($row['patch_description']) { ?>
						<p class="lead">
							<?php echo $row['patch_description'];?>
						</p>
					<?php } ?>
					<div class="well margin-top">
						<h4 class="strong">Possible solution - patch code</h4>
						<code>
							<?php echo $row['patch_code'];?>
						</code>
					</div>
					<table class="table table-striped table-bordered margin-top">
						<thead>
							<tr>
								<th class="col-sm-2 text-center">Ticket ID</th>
								<th class="col-sm-10">Ticket description</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="text-center"><?php echo $row['ticket_id'];?></td>
								<td>
									<?php echo $row['ticket_description'];?><br/>
									<em><small><strong>Ticket submitted:</strong> <?php echo $row['ticket_date'];?></small></em>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	<?php } ?>
<?php } ?>