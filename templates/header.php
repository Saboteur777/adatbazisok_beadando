<header>
	<div class="jumbotron">
		<div class="container">
			<div class="row">
				<div class="visible-sm col-xs-6 col-xs-offset-3 vcenter">
					<?php include(get_template_part('assets/images/logo.svg'));?>
				</div>
				<div class="col-xs-12 col-md-7 vcenter">
					<h1><?php echo $page_title;?></h1>
					<p><?php echo $page_subtitle;?></p>
				</div>
				<div class="hidden-sm col-md-4 vcenter">
					<?php include(get_template_part('assets/images/logo.svg'));?>
				</div>
			</div>
		</div>
	</div>
</header>