<footer>
	<div class="container">
		<div class="row">
			<div class="col-xs-6 col-sm-2">
				<?php include(get_template_part('assets/images/logo.svg'));?>
			</div>
			<div class="col-xs-6 col-sm-4 col-sm-offset-1">
				<h3>Contact us!</h3>
				<ul class="list-unstyled">
					<li class="padding-top"><i class="fa fa-2x fa-phone padding-right vcenter"></i> <span class="vcenter">+36 20 923 8883</span></li>
					<li class="padding-top"><i class="fa fa-2x fa-envelope padding-right vcenter"></i> <a class="vcenter" href="mailto:otto@webmenedzser.hu">Drop us a message!</a></li>
				</ul>
			</div>
			<div class="col-xs-12 col-sm-5 text-right">
				<h3>Lorem ipsum dolor sit amet!</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo corporis dicta laborum molestias ab impedit iure incidunt est nam consequatur numquam, rem quibusdam tenetur fuga ullam architecto quam earum in.</p>
				<p class="poweredby">
					<a title="Honlapkészítés és reszponzív weboldalkészítés" href="https://webmenedzser.hu" target="_blank">
					<!-- Footer link UTM preparation! -->
						<img src="/assets/images/webmngr-white.svg" alt="Honlapkészítés és reszponzív weboldalkészítés">
					</a>
				</p>
			</div>
		</div>
	</div>
</footer>
<script src="/assets/js/scripts.js"></script>