<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="/index"><?php include(get_template_part('assets/images/logo.svg'));?></a>
		</div>
		<?php 

			$left_menu = array(
				"Home" => "index",
				"Our Devs" => "devs",
				"Roles" => "roles"
			);

			$right_menu = array(
				"Tickets" => "tickets",
				"Patches" => "patches",
				"Administration" => "administration"
			);

			$menus = array(
				"left" => $left_menu,
				"right" => $right_menu
			);
		?>

		<?php foreach ($menus as $position => $menu) { ?>
			<ul class="nav navbar-nav navbar-<?php echo $position;?>">
				<?php foreach ($menu as $title => $url) { ?>
					<li><a href="/<?php echo $url;?>"><?php echo $title;?></a></li>
				<?php } ?>
			</ul>
		<?php } ?>
	</div>
</nav>