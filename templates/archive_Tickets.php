<?php $rows = table_content($type, $conn); ?>
<?php if ($rows) { ?>
	<?php foreach ($rows as $row) { ?>
		<div class="row">
			<div class="col-xs-8 col-xs-offset-2">
				<?php include(get_template_part('admin/edit_menu.php'));?>
				<div class="padding-top-4x padding-bottom-4x">
					<h2>
						<?php echo $row['name'];?><br/>
					</h2>
					<?php if ($row['description']) { ?>
						<p class="lead">
							<?php echo $row['description'];?>
						</p>
					<?php } ?>
				</div>
			</div>
		</div>
	<?php } ?>
<?php } ?>