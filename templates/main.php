<div id="home">
	<div class="container-fluid">
		<div class="row counts">
			<div class="count Devs" data-mh="columns">
				<div class="count-number"><?php echo count(table_content('Devs', $conn));?></div>
			</div>
			<div class="description" data-mh="columns">
				<div class="description-text">
					<h2>Devs</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto veritatis nesciunt impedit molestiae ex illo, asperiores ipsam ipsa perferendis sit repellat exercitationem assumenda nihil minus praesentium non nulla cumque eveniet.</p>
				</div>
			</div>
		</div>
		<div class="row counts">
			<div class="count Tickets" data-mh="columns">
				<div class="count-number"><?php echo count(table_content('Tickets', $conn));?></div>
			</div>
			<div class="description" data-mh="columns">
				<div class="description-text">
					<h2>Tickets</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto veritatis nesciunt impedit molestiae ex illo, asperiores ipsam ipsa perferendis sit repellat exercitationem assumenda nihil minus praesentium non nulla cumque eveniet.</p>
				</div>
			</div>
		</div>
		<div class="row counts">
			<div class="count Roles" data-mh="columns">
				<div class="count-number"><?php echo count(table_content('Roles', $conn));?></div>
			</div>
			<div class="description" data-mh="columns">
				<div class="description-text">
					<h2>Roles</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto veritatis nesciunt impedit molestiae ex illo, asperiores ipsam ipsa perferendis sit repellat exercitationem assumenda nihil minus praesentium non nulla cumque eveniet.</p>
				</div>
			</div>
		</div>
		<div class="row counts">
			<div class="count Patches" data-mh="columns">
				<div class="count-number"><?php echo count(table_content('Patches', $conn));?></div>
			</div>
			<div class="description" data-mh="columns">
				<div class="description-text">
					<h2>Patches</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto veritatis nesciunt impedit molestiae ex illo, asperiores ipsam ipsa perferendis sit repellat exercitationem assumenda nihil minus praesentium non nulla cumque eveniet.</p>
				</div>
			</div>
		</div>
		<div class="row text-center bg-primary">
			<div class="col-xs-12">
				<h2>Do you have questions?</h2>
				<p class="lead">Feel free to contact us below!</p>
			</div>
		</div>
	</div>
</div>