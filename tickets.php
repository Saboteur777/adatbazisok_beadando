<!DOCTYPE html>
	<?php $page_title = "Open Tickets";?>
	<?php $page_subtitle = "Here you can check all our open tickets";?>
	<?php $type = "Tickets";?>
	<?php include($_SERVER["DOCUMENT_ROOT"].'/templates/head.php');?>
	<body>
		<?php include(get_template_part('templates/nav.php'));?>
		<?php include(get_template_part('templates/header.php'));?>
		<?php include(get_template_part('templates/archive.php'));?>
	</body>
	<?php include(get_template_part('templates/footer.php'));?>
</html>