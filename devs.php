<!DOCTYPE html>
	<?php $page_title = "Our Devs";?>
	<?php $page_subtitle = "The Pantheon of our Staff";?>
	<?php $type = "Devs";?>
	<?php include($_SERVER["DOCUMENT_ROOT"].'/templates/head.php');?>
	<body>
		<?php include(get_template_part('templates/nav.php'));?>
		<?php include(get_template_part('templates/header.php'));?>
		<?php include(get_template_part('templates/archive.php'));?>
	</body>
	<?php include(get_template_part('templates/footer.php'));?>
</html>